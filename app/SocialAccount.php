<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    public function app_user()
    {
        return $this->belongsTo(AppUser::class,'app_user_id');
    }
}
