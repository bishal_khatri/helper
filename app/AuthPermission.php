<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthPermission extends Model
{
    protected $primaryKey = 'ap_id';
}
