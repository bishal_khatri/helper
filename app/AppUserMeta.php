<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppUserMeta extends Model
{
    protected $fillable = ['app_user_id'];
}
