<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferApplication extends Model
{
    protected $primaryKey = 'oa_id';

    public function app_user()
    {
        return $this->belongsTo(AppUser::class,'app_user_id');
    }
}
