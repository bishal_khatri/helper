<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $dates = ['start_date','end_date'];
    public function author()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function offer()
    {
        return $this->belongsTo(User::class,'offer_by');
    }

    public function application()
    {
        return $this->hasMany(OfferApplication::class,'offer_id');
    }
}
