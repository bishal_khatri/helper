<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $primaryKey = 'f_id';

    public function university()
    {
        return $this->belongsTo(University::class,'university_id');
    }

    public function notes()
    {
        return $this->hasMany(Note::class,'faculty_id');
    }
}
