<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class AppUser extends Authenticatable implements JWTSubject
{
    protected $primaryKey = 'app_user_id';

    protected $hidden = [
        'password'
    ];
    use Notifiable;

    public function social_account()
    {
        return $this->hasMany(SocialAccount::class,'user_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function meta()
    {
        return $this->hasOne(AppUserMeta::class,'app_user_id');
    }
}
