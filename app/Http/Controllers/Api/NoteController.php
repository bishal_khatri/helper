<?php

namespace App\Http\Controllers\Api;

use App\Faculty;
use App\Helper\Tools;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoteController extends Controller
{
    public function getAllNoteByFacultyId($id){
        if(!isset($id)){
            if(empty($id)){
                $responseData = Tools::setResponse(true,'Missing Parameter','','');
                return response()->json($responseData);
            }
        }

        $notes = Faculty::where('f_id',$id)->with('notes')->first();
        if(empty($notes)){
            $responseData = Tools::setResponse(true,'Notes not found | Empty','','');
            return response()->json($responseData);
        }

        $responseData = Tools::setResponse(false,'Note Listing Successfull',$notes,'');
        return response()->json($responseData);
    }
}
