<?php

namespace App\Http\Controllers\Api;

use App\Helper\Tools;
use App\Offer;
use App\OfferApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfferController extends Controller
{
    public function offerListing()
    {
        $now = date('Y-m-d H:i:s');
        $data = Offer::where('is_approved',1)->where('end_date','>', $now)->orWhereNull('end_date')->with('offer')->get();
        $responseData = Tools::setResponse(false,'offer_listed_successfully',$data,'');

        return response()->json($responseData);
    }

    public function apply(Request $request)
    {
        $this->validate($request, [
            'offer_id' => 'required',
            'app_user_id' => 'required'
        ]);

        $offer = Offer::find($request->offer_id);
        if (is_null($offer)){
            $responseData = Tools::setResponse(true,'offer_not_found','','');
        }else{
            $check_application = OfferApplication::where('app_user_id', $request->app_user_id)->where('offer_id', $request->offer_id)->get();
            if ($check_application->count() > 0){
                $responseData = Tools::setResponse(true,'already_applied','','');
            }else{
                $apply = new OfferApplication();
                $apply->app_user_id = $request->app_user_id;
                $apply->offer_id = $offer->id;
                $apply->save();
                $responseData = Tools::setResponse(false,'offer_applied_successfully','','');
            }
        }

        return response()->json($responseData);
    }
}
