<?php

namespace App\Http\Controllers\Api;

use App\Helper\Tools;
use App\Job;
use App\JobApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JobController extends Controller
{
    public function jobListing()
    {
        $now = date('Y-m-d');
        $data = Job::where('deadline','>',$now)->with('job_provider')->get();
        $responseData = Tools::setResponse(false,'Job Listing Successfull',$data,'');

        return response()->json($responseData);
    }

    public function apply(Request $request)
    {
        $this->validate($request, [
            'job_id' => 'required',
            'app_user_id' => 'required'
        ]);

        $job = Job::find($request->job_id);
        if (is_null($job)){
            $responseData = Tools::setResponse(true,'job_not_found','','');
        }else{
            $check_application = JobApplication::where('app_user_id', $request->app_user_id)->where('job_id', $request->job_id)->get();
            if ($check_application->count() > 0){
                $responseData = Tools::setResponse(true,'already_applied','','');
            }else{
                $apply = new JobApplication();
                $apply->app_user_id = $request->app_user_id;
                $apply->job_id = $job->id;
                $apply->save();
                $responseData = Tools::setResponse(false,'job_applied_successfully','','');
            }
        }

        return response()->json($responseData);
    }
}
