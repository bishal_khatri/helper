<?php

namespace App\Http\Controllers\Api;

use App\Helper\Tools;
use App\University;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UniversityController extends Controller
{
    public function getAllUniversity()
    {
        $university = University::with('faculty')->where('status',1)->get();

        $responseData = Tools::setResponse(false,'University Listing Successfull',$university,'');
        return response()->json($responseData);
    }

    public function getFacultyByUniversityId(){
        ///check if id is sent or not.
        if(!isset($_GET['u_id'])){
            if(empty($_GET['u_id'])){
                $responseData = Tools::setResponse(true,'Missing Parameter','','');
                return response()->json($responseData);
            }
        }

        $university = University::where('u_id',$_GET['u_id'])->with('faculty','college','faculty.notes')->first();

        $responseData = Tools::setResponse(false,'Faculty listing with its related notes by University Id  successfull',$university,'');
        return response()->json($responseData);

    }

}
