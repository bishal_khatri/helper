<?php

namespace App\Http\Controllers\Api;

use App\AppUser;
use App\Helper\Tools;
use App\Http\Controllers\Api\Auth\IssueTokenTrait;
use App\SocialAccount;
use App\Traits\PassportToken;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;

class AuthenticationController extends Controller
{
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return Tools::setResponse(true,'missing_parameter','');
        }
        $user = AppUser::where('email', $request->email)->with('meta')->first();
        if (!is_null($user)){
            if (Hash::check($request->password,$user->password)){
                try {
                    if (! $token = JWTAuth::fromUser($user)) {
                        $responseData  = Tools::setResponse(false,'invalid_credentials',[]);
                        return response()->json($responseData);
                    }
                } catch (JWTException $e) {
                    $responseData  = Tools::setResponse(false,'could_not_create_token',[]);
                    return response()->json($responseData);
                }

                $data = [
                    'token_type' => 'bearer',
                    'token' => $token,
                    'expires_in' => Config::get('jwt.ttl') * 60,
                    'user_data' => $user
                ];
                $responseData  = Tools::setResponse(false,'login_success',$data);
            }
            else{
                $responseData  = Tools::setResponse(false,'invalid_credentials','');
            }
        }else{
            $responseData  = Tools::setResponse(false,'invalid_credentials','');
        }


        return response()->json($responseData);
    }

    public function socialLogin(Request $request)
    {
        $this->validate($request, [
            'provider' => 'required',
            'provider_user_id' => 'required',
            'email' => 'required',
        ]);
        $app_user = AppUser::where('email', $request->email)->with('meta')->first();
        $check_social_account = SocialAccount::where('provider', $request->provider)->where('provider_user_id', $request->provider_user_id)->first();
        if(!$check_social_account){
            if (!$app_user){
                $app_user = new AppUser();
                $app_user->full_name = $request->full_name;
                $app_user->email = $request->email;
                $app_user->save();
            }
            $new_social = new SocialAccount();
            $new_social->app_user_id = $app_user->app_user_id;
            $new_social->provider = $request->provider;
            $new_social->provider_user_id = $request->provider_user_id;
            $new_social->save();
        }else{
            $app_user = AppUser::where('app_user_id',$check_social_account->app_user_id)->with('meta')->first();
        }

        $token = JWTAuth::fromUser($app_user);
        $data = [
            'token_type' => 'bearer',
            'token' => $token,
            'expires_in' => Config::get('jwt.ttl') * 60,
            'user_data' => $app_user
        ];
        $responseData  = Tools::setResponse(false,'login_success',$data);
        return response()->json($responseData);
    }

    public function refresh_token(){
        $token = JWTAuth::getToken();
        if(!$token){
            $responseData  = Tools::setResponse(false,'token_not_provided','');
        }else{
            try{
                $token = JWTAuth::refresh($token);
                $data = [
                    'token_type' => 'bearer',
                    'token' => $token,
                    'expires_in' => Config::get('jwt.ttl') * 60,
                ];
                $responseData  = Tools::setResponse(false,'success',$data);
            }catch(Exception $e){
                if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                    $responseData = Tools::setResponse(true, 'token_can_no_longer_be_refreshed', '');
                }
                elseif ($e instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException){
                    $responseData = Tools::setResponse(true, 'token_has_been_blacklisted', '');
                }
                else{
                    $responseData = Tools::setResponse(true, 'something_went_wrong', '');
                }
            }
        }
        return response()->json($responseData);
    }

}
