<?php

namespace App\Http\Controllers\Api;

use App\AppUser;
use App\AppUserMeta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Helper\Tools;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Config;


class AppUserController extends Controller
{
    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'email' => 'required|email|unique:app_users,email',
            'password' =>'required',
            'confirm_password' => 'required'
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse(true,'missing_parameter','','');
            return response()->json($responseData);
        }

        if ($request->password != $request->confirm_password) {
            $responseData = Tools::setResponse(true,'password_mismatch','','');
            return response()->json($responseData);
        }

        $app_user = new AppUser();
        $app_user->full_name = $request->full_name;
        $app_user->email = $request->email;
        $app_user->password = Hash::make($request->password);
        $app_user->save();
        $token = JWTAuth::fromUser($app_user);

        $data = [
            'token_type' => 'bearer',
            'token' => $token,
            'expires_in' => Config::get('jwt.ttl') * 60,
            'user_data' => $app_user

        ];


        $responseData = Tools::setResponse(false,'app_user_registered_successfully',$data,'');
        return response()->json($responseData);

    }

    public function updateProfile(Request $request){

        $validator = Validator::make($request->all(), [
            'app_user_id' => 'required',
            'email' => 'required|email|unique:app_users,email',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse(true,'missing_parameter','','');
            return response()->json($responseData);
        }

        $app_user = AppUser::find($request->app_user_id);
//        $app_user->full_name = $request->full_name;
//        $app_user->email = $request->email;
        $app_user->password = $request->password;

        $app_user->phone = $request->phone;
        $app_user->university_id = $request->university_id;

        $app_user->college_id = $request->college_id;
        $app_user->faculty_id = $request->faculty_id;

        $app_user->semester = $request->semester;
        $app_user->created_at =date('Y-m-d H:i:s');
        $app_user->save();

        $responseData = Tools::setResponse(false,'profile_updated_successfully','','');
        return response()->json($responseData);
    }

    public function update_user_meta(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_user_id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse(true,'missing_parameter','','');
            return response()->json($responseData);
        }

        $check_app_user = AppUser::find($request->app_user_id);
        if ($check_app_user){
            $meta = AppUserMeta::firstOrNew(['app_user_id'=>$request->app_user_id]);
            if ($request->hasFile('cv')){
                $path = $request->file('cv')->store('app_user_meta/'.$request->app_user_id, 'public');
                $meta->cv = $path;
            }
            $meta->save();
            $responseData = Tools::setResponse(false,'app_user_updated_successfully','','');
        }else{
            $responseData = Tools::setResponse(true,'app_user_not_found','','');
        }
        return response()->json($responseData);
    }

    public function update_profile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'app_user_id' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData = Tools::setResponse(true,'missing_parameter','','');
            return response()->json($responseData);
        }

        $user = AppUser::find($request->app_user_id);
        if ($user){
            if ($request->hasFile('image')){
                $path = $request->file('image')->store('app_user_meta/'.$request->app_user_id, 'public');
                $user->image = $path;
            }
            $user->full_name = $request->full_name;
            $user->phone = $request->phone;
            $user->university_id = $request->university_id;
            $user->college_id = $request->college_id;
            $user->faculty_id = $request->faculty_id;
            $user->semester = $request->semester;
            $user->save();
            $responseData = Tools::setResponse(false,'app_user_updated_successfully','','');
        }else{
            $responseData = Tools::setResponse(true,'app_user_not_found','','');
        }
        return response()->json($responseData);
    }


}
