<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Note;
use App\Solution;
use App\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use PDFMerger;

class NoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['university'] = University::with('faculty')->get();
        return view('files.index',$data);
    }

    public function files($fid)
    {
        $data['faculty'] = Faculty::where('f_id', $fid)->with('university')->firstOrFail();
        $data['notes'] = Note::where('faculty_id',$fid)->get()->groupBy('semester');
        if ($data['faculty']->course_type == 'semester' AND $data['faculty']->count == 8 ){
            return view('files.notes.semester-8',$data);
        }else{
            dd('Under Construction');
        }
    }

    public function files_ajax($faculty_id)
    {
        $data['faculty'] = Faculty::where('f_id', $faculty_id)->with('university')->firstOrFail();
        $data['notes'] = Note::where('faculty_id',$faculty_id)->get()->groupBy('semester');
        return response()->json($data);
    }

    public function upload_files(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'semester' => 'required',
            'file' => 'required'
        ]);

        $note = new Note();
        $note->faculty_id = $request->faculty_id;
        $note->semester = $request->semester;
        $note->title = $request->title;
        $note->subject_code = strtoupper($request->subject_code);

        $pdf = new PDFMerger();

        foreach ($request->file('file') as $value){
            $pdf->addPDF($value, 'all');
        }

        $file_name = Str::random(25).'.pdf';
        File::isDirectory(storage_path('app/public/notes/'.$request->faculty_id)) or File::makeDirectory(storage_path('app/public/notes/'.$request->faculty_id), 0777, true, true);
        $pathForTheMergedPdf = storage_path('app/public/notes/'.$request->faculty_id . '/') . $file_name;
        $path = 'notes/'.$request->faculty_id.'/'.$file_name;
        $pdf->merge('file',$pathForTheMergedPdf);

        $note->file = $path;
        $note->save();

        return redirect()->back();
    }

    public function delete_file(Request $request)
    {
        $file = Note::find($request->id);
        \Storage::delete('public/'.$file->file);
        $file->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'File deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return json_encode(array('succss'=>'true'));
    }
}
