<?php

namespace App\Http\Controllers;

use App\Offer;
use App\OfferApplication;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class OfferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['users'] = User::where('user_type', 'user')->get();
        $data['offers'] = Offer::with(['author','offer','application'])->get();
        return view('offers.index',$data);

        // FOR ADMIN
//        if (Auth::user()->user_type=='admin'){
//            $data['users'] = User::where('user_type', 'user')->get();
//            $data['offers'] = Offer::with(['author','offer','application'])->get();
//            return view('offers.index-admin',$data);
//        }
//        // FOR USER
//        elseif (Auth::user()->user_type=='user'){
//            $data['offers'] = Offer::where('offer_by',Auth::user()->id)->with(['offer','application'])->get();
//            return view('offers.index',$data);
//        }
//        else{
//            abort(403);
//        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_type' => 'required',
            'title' => 'required|max:255',
//            'offer_by' => 'required'
        ]);

        $offer = new Offer();
        $offer->title = $request->title;

        if(!empty($request->start)){
            $offer->start_date = date("Y-m-d H:i:s",strtotime($request->start));
        }

        if(!empty($request->end)){
            $offer->end_date = date("Y-m-d H:i:s",strtotime($request->end));
        }

        $offer->description = $request->description;
        if ($request->hasFile('image')) {
            $offer->image = $request->file('image')->store('offers', 'public');
        }
        if (Auth::user()->user_type=='user'){
            $offer->offer_by = Auth::user()->id;
            $offer->is_approved = 0;
        }else{
            $offer->offer_by = $request->offer_by;
            $offer->is_approved = 1;
        }
        $offer->created_by = Auth::user()->id;
        $offer->save();

        return redirect()->back();
    }

    public function edit(Request $request){
        $request->validate([
            'title' => 'required|max:255',
        ]);

        $offer = Offer::find($request->offer_id);
        $offer->title = $request->title;

        if(!empty($request->start)){
            $offer->start_date = date("Y-m-d H:i:s",strtotime($request->start));
        }

        if(!empty($request->end)){
            $offer->end_date = date("Y-m-d H:i:s",strtotime($request->end));
        }

        if(!empty($request->description)){
            $offer->description = $request->description;
        }

        //if new image is uploaded.
        if(isset($request->image)){
            Storage::delete('public/'.$offer->image);
            $offer->image = $request->file('image')->store('offers', 'public');
        }
        $offer->updated_at = date('Y-m-d H:i:s');
        $offer->save();

        return redirect()->back();
    }

    public function approve($id){
        $offer = Offer::find($id);
        $offer->is_approved = 1;
        $offer->save();

        return redirect()->back();
    }

    public function reject($id){
        $offer = Offer::find($id);
        $offer->is_approved = 2;
        $offer->save();

        return redirect()->back();
    }

    public function canEditOffer($id,$action){
        $offer = Offer::find($id);

        if($action == "can_edit"){
            $offer->can_edit = 1;
        }
        else{
            $offer->can_edit = 0;
        }
        $offer->save();


        return redirect()->back();
    }

    public function delete(Request $request){
        $offer = Offer::find($request->id);
        Storage::delete('public/'.$offer->image);
        $offer->delete();
    }

    // APPLICATIONS

    public function get_applications($offer_id)
    {
        $data['offer'] = Offer::find($offer_id);
        $data['applications'] = OfferApplication::where('offer_id', $offer_id)->with('app_user')->get();

        return view('offers.applications', $data);
    }
}
