<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SetupController extends Controller
{
    public function setup()
    {
        return view('setup');
    }

    public function setup_run(Request $request)
    {
        $message = [];
        if ($request->cache_clear=="on") {
            Artisan::call('cache:clear');
            $message[] = "Cache Cleared";
        }
        if ($request->view_clear=="on") {
            Artisan::call('view:clear');
            $message[] = "View Cleared";
        }

        if ($request->config_cache=="on") {
            Artisan::call('config:cache');
            $message[] = "Config Cached";
        }

        if ($request->migrate_refresh=="on") {
            Artisan::call('migrate:refresh');
            $message[] = "Migration Refreshed";
        }

        if ($request->migrate_fresh=="on") {
            Artisan::call('migrate:fresh');
            $message[] = "Fresh Migration";
        }

        if ($request->migrate=="on") {
            Artisan::call('migrate');
            $message[] = "Database Migrated";
        }

        if ($request->db_seed=="on") {
            Artisan::call('db:seed');
            $message[] = "Database Seeded";
        }

        if ($request->storage_link=="on") {
            Artisan::call('storage:link');
            $message[] = "Storage Linked";
        }

        if ($request->passport_install=="on") {
            Artisan::call('passport:install');
            $message[] = "Passport Installed";
        }

        if ($request->seed_permission=="on") {
            Artisan::call('db:seed --class=PermissionTableSeeder');
            $message[] = "Permission Table Seeded";
        }

        return redirect()->back()->with('message',$message);
    }
}
