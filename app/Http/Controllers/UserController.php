<?php

namespace App\Http\Controllers;

use App\AuthPermission;
use App\AuthPermissionUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['user_type'] = 'Users';
        $data['users'] = User::where('user_type','user')->get();
        return view('auth.index', $data);
    }

    public function admin_index()
    {
        $data['user_type'] = 'Admin';
        $data['users'] = User::where('user_type','admin')->get();
        return view('auth.index', $data);
    }

    public function getPermission()
    {
        $data['permissions'] = AuthPermission::all();
        return view('auth.permission.index', $data);
    }

    public function savePermission(Request $request)
    {
        $prem = new AuthPermission();
        $prem->display_name = $request->display_name;
        $prem->codename = $request->codename;
        $prem->save();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Permission successfully saved.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function assign_permission($id)
    {
        $data['user'] = User::find($id);
        $active_user_permission = AuthPermissionUser::where('user_id',$data['user']->id)->get();
        $u_prem = array();
        foreach ($active_user_permission as $val){
            $u_prem[] = $val->permission_id;
        }
        $data['active_user_permission'] = $u_prem;
        $data['permissions'] = AuthPermission::all();
        return view('auth.permission.assign',$data);
    }

    public function assign_permission_store(Request $request)
    {
        AuthPermissionUser::where('user_id', $request->user_id)->delete();
        if (!empty($request->selected_permission)) {
            foreach ($request->selected_permission as $value) {
                $pu = new AuthPermissionUser();
                $pu->user_id = $request->user_id;
                $pu->permission_id = $value;
                $pu->save();
            }
        }
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Permission assigned successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        if (isset($request->assign)){
            return redirect()->back();
        }else{
            return redirect()->route('user.index');
        }
    }

    public function permission_view(Request $request)
    {
        $permissions = AuthPermissionUser::where('user_id',$request->id)->with('permission')->get();
        $data = array();
        foreach ($permissions as $val){
            $data[] = "<li class='list-group-item'>".$val->permission->display_name."</li>";
        }
        if (!empty($data)){
            $return = $data;
        }else{
            $return = "<li class='list-group-item'>No permission has been assigned.</li>";
        }

        return $return;
    }

    public function view_profile($id)
    {
        return json_encode(User::findOrFail($id));
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'user_type' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->user_type = $request->user_type;
        $user->contact = $request->contact;
        $user->password = Hash::make($request->password);
        if ($request->hasFile('image')){
            $user->image = $request->file('image')->store('user_profile', 'public');
        }

        $user->save();

        return redirect()->back();
    }
}
