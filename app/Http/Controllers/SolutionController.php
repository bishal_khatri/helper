<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\Solution;
use Illuminate\Http\Request;
use Auth;

class SolutionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function files($fid)
    {
        $data['faculty'] = Faculty::where('f_id', $fid)->with('university')->firstOrFail();
        $data['solutions'] = Solution::where('faculty_id',$fid)->get()->groupBy('semester');
        if ($data['faculty']->course_type == 'semester' AND $data['faculty']->count == 8 ){
            return view('files.solution.semester-8',$data);
        }else{
            dd('Under Construction');
        }
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'semester' => 'required',
            'file' => 'required'
        ]);
        $note = new Solution();
        $note->faculty_id = $request->faculty_id;
        $note->semester = $request->semester;
        $note->title = $request->title;
        $note->subject_code = strtoupper($request->subject_code);
        $path = $request->file('file')->store('solution/'.$request->faculty_id, 'public');
        $note->file = $path;
        $note->author = Auth::user()->id;
        $note->save();

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $file = Note::find($request->id);
        \Storage::delete('public/'.$file->file);
        $file->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'File deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return json_encode(array('succss'=>'true'));
    }

}
