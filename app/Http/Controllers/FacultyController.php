<?php

namespace App\Http\Controllers;

use App\Faculty;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function update(Request $request){
        $this->validate($request,[
            'university_id' => 'required',
            'title' => 'required|max:200',
            'course_type' => 'required|max:200',
            'count' => 'required',
        ]);
        $faculty = Faculty::find($request->faculty_id);
        $faculty->university_id = $request->university_id;
        $faculty->title = $request->title;
        $faculty->course_type = $request->course_type;
        $faculty->count = $request->count;
        $faculty->save();

        return redirect()->back();
    }

    public function delete(Request $request){
        $faculty = Faculty::find($request->id);
        $faculty->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'faculty deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return response()->json(array('success'=>'true'));
    }

    public function change_status($id)
    {
        $faculty = Faculty::find($id);
        if ($faculty->status==1){
            $faculty->status = 0;
        }else{
            $faculty->status = 1;
        }
        $faculty->save();
        return redirect()->back();
    }
}
