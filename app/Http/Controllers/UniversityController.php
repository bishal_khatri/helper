<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\University;
use Illuminate\Http\Request;

class UniversityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['university'] = University::all();
        $data['faculty'] = Faculty::with('university')->orderBy('university_id')->get();
        return view('university.index',$data);
    }

    public function save(Request $request)
    {
        if ($request->type=='university') {
            $this->validate($request,[
                'name' => 'required|max:200'
            ]);
            $university = new University();

            if ($request->hasFile('logo')){
                $path = $request->file('logo')->store('university', 'public');
                $university->logo = $path;
            }
            $university->name = $request->name;
            $university->status = 0;
            $university->save();
        }
        elseif ($request->type=='faculty'){
            $this->validate($request,[
                'university_id' => 'required',
                'title' => 'required|max:200',
                'course_type' => 'required|max:200',
                'count' => 'required',
            ]);
            $faculty = new Faculty();
            $faculty->university_id = $request->university_id;
            $faculty->title = $request->title;
            $faculty->course_type = $request->course_type;
            $faculty->count = $request->count;
            $faculty->save();
        }

        return redirect()->back();
    }

    public function change_status($id)
    {
        $uni = University::find($id);
        if ($uni->status==1){
            $uni->status = 0;
        }else{
            $uni->status = 1;
        }
        $uni->save();
        return redirect()->back();
    }

    public function delete(Request $request){
        $file = University::where('u_id',$request->id)->first();
        \Storage::delete('public/'.$file->logo);
        $file->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'University deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return json_encode(array('success'=>'true'));
    }
}
