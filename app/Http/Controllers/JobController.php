<?php

namespace App\Http\Controllers;

use App\Job;
use App\JobApplication;
use App\User;
use Illuminate\Http\Request;
use Auth;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['jobs'] = Job::with(['job_provider','applications'])->get();
        $data['providers'] = User::where('user_type','user')->get();
        return view('job.index', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'deadline' => 'required',
            'job_type' => 'required',
            'location' => 'required|max:200',
            'designation' => 'required|max:255',
        ]);
//        dd($request->all());
        $job = new Job();
        $job->title = $request->title;
        $job->deadline = $request->deadline;
        $job->job_type = $request->job_type;
        $job->location = $request->location;
        $job->designation = $request->designation;
        $job->description = $request->description;
        if (isset($request->provider)){
            $job->provider = $request->provider;
        }else{
            $job->provider = Auth::user()->id;
        }

        $job->save();
        return redirect()->back();
    }

    public function get_applications($job_id)
    {
        $data['offer'] = Job::find($job_id);
        $data['applications'] = JobApplication::where('job_id', $job_id)->with('app_user')->get();

        return view('job.applications', $data);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'deadline' => 'required',
            'job_type' => 'required',
            'designation' => 'required|max:255',
            'location' => 'required|max:200'
        ]);


        $job = Job::findOrFail($request->job_id);
        $job->title = $request->title;
        $job->deadline = $request->deadline;
        $job->designation = $request->designation;
        $job->location = $request->location;
        $job->description = $request->description;
        
        if (isset($request->provider)){
            $job->provider = $request->provider;
        }else{
            $job->provider = Auth::user()->id;
        }

        $job->save();

        return redirect()->back();
    }
}
