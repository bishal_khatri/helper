<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data['user'] = User::find(\Auth::user()->id);
        return view('profile.index',$data);
    }

    public function update(Request $request){
        $request->validate([
            'name' => 'required',
            'old_password' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);


        $user = User::find(\Auth::user()->id);


        if (Hash::check($request->old_password, $user->password)) {
            // The passwords match...
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->name = $request->name;

            if(isset($request->image)){
                if($user->image != null){
                    \Storage::delete('public/'.$user->image);
                }
                $user->image = $request->file('image')->store('user/'.$user->id, 'public');

            }
           $user->save();

           return redirect()->back();


        }
        else{
            \Session::flash('error','Password does not match');
            return redirect()->back()->withErrors(['pwd_mismatch' => 'Old Password does not match.']);
        }
    }
}
