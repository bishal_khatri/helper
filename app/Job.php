<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $dates = ['deadline'];

    public function job_provider()
    {
        return $this->belongsTo(User::class,'provider');
    }

    public function applications()
    {
        return $this->hasMany(JobApplication::class,'job_id');
    }
}
