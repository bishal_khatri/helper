<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use App\AuthPermission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->userAccessPolicies();
    }

    public function userAccessPolicies()
    {
        Gate::before(function($user, $ability) {
            if($user->user_type == 'admin') {
                return true;
            }
        });

//        Gate::define('admin', function($user){
//            if ($user->user_type=='admin'){
//                return true;
//            }
//            return false;
//        });
//
//        Gate::define('user', function($user){
//            if ($user->user_type=='user'){
//                return true;
//            }
//            return false;
//        });

        if (Schema::hasTable('auth_permissions')){
            $permissions = AuthPermission::all();
            foreach ($permissions as $value){
                $role = $value->codename;
                Gate::define($role, function($user) use($role){
                    return $user->hasAccess($role);
                });

            }
        }
    }

}
