<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthPermissionUser extends Model
{
    protected $primaryKey = 'apu_id';

    public function permission()
    {
        return $this->belongsTo(AuthPermission::class,'permission_id');
    }
}
