<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $primaryKey = 'u_id';

    public function faculty()
    {
        return $this->hasMany(Faculty::class,'university_id');
    }

    public function college()
    {
        return $this->hasMany(College::class,'university_id');
    }


}
