<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    public function app_user()
    {
        return $this->belongsTo(AppUser::class,'app_user_id');
    }

//    public function applicant_cv()
//    {
//        $this->hasOne(AppUserMeta::class,'app_user_id','app_user_id');
//    }

}
