<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware'=>'api'], function(){
    Route::post('login', 'Api\AuthenticationController@authenticate');
    Route::post('user/register', 'Api\AppUserController@register');
    Route::post('social_login', 'Api\AuthenticationController@socialLogin');
    Route::get('refresh_token', 'Api\AuthenticationController@refresh_token');

    // Get university info and list all university
    Route::get('university/list', 'Api\UniversityController@getAllUniversity');
    Route::get('university/faculty', 'Api\UniversityController@getFacultyByUniversityId');

    // Fetch notes based on faculty id
    Route::get('offer/list', 'Api\OfferController@offerListing');
    Route::get('notes/{faculty_id}', 'Api\NoteController@getAllNoteByFacultyId');

    // AUTH MIDDLEWARE
    Route::group(['middleware' => 'jwt'], function () {
        Route::post('offer/apply', 'Api\OfferController@apply');
        Route::post('job/apply', 'Api\JobController@apply');
        Route::post('update_profile', 'Api\AppUserController@update_profile');
        Route::post('update_user_meta', 'Api\AppUserController@update_user_meta');
    });

    // JOB LISTING
    Route::get('job/list', 'Api\JobController@jobListing');
});



