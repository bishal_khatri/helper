<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(!defined('STATIC_DIR')) define('STATIC_DIR','public/');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::post('/profile/update', 'ProfileController@update')->name('profile-update');

Route::group(['prefix' => 'university','as'=>'university.'], function () {
    Route::get('/', ['uses'=>'UniversityController@index','as'=>'index'])->middleware("can:university-list");
    Route::Post('/save', ['uses'=>'UniversityController@save','as'=>'save'])->middleware("can:university-list");
    Route::Post('/delete', ['uses'=>'UniversityController@delete','as'=>'delete'])->middleware("can:university-list");
    Route::get('/change_status/{id}', ['uses'=>'UniversityController@change_status','as'=>'change_status'])->middleware("can:university-list");
});

Route::group(['prefix' => 'faculty', 'as' => 'faculty.'],function(){
    Route::post('/delete',['uses' => 'FacultyController@delete','as' => 'delete'])->middleware("can:university-list");
    Route::post('/update',['uses' => 'FacultyController@update','as' => 'update'])->middleware("can:university-list");
    Route::get('/change_status/{id}', ['uses'=>'FacultyController@change_status','as'=>'change_status'])->middleware("can:university-list");
});

Route::group(['prefix' => 'offers','as'=>'offers.'], function () {
    Route::get('/', ['uses'=>'OfferController@index','as'=>'index'])->middleware("can:offer-list");
    Route::post('/save', ['uses'=>'OfferController@store','as'=>'save'])->middleware("can:offer-add");
    Route::get('/application/{offer_id}', ['uses'=>'OfferController@get_applications','as'=>'get_applications'])->middleware("can:offer-applications-list");
    Route::post('/edit', ['uses'=>'OfferController@edit','as'=>'edit'])->middleware("can:offer-edit");

    //Admin Routes
    Route::post('/delete', ['uses'=>'OfferController@delete','as'=>'delete'])->middleware("can:offer-delete");
    Route::get('/approve/{id}', ['uses'=>'OfferController@approve','as'=>'approve']);
    Route::get('/reject/{id}', ['uses'=>'OfferController@reject','as'=>'reject']);
//    Route::get('/edit/offer/{id}/{action}', ['uses'=>'OfferController@canEditOffer','as'=>'can-edit-offer']);
});

Route::group(['prefix' => 'files','as'=>'files.'], function () {
    Route::get('/', ['uses'=>'NoteController@index','as'=>'index']);

    Route::group(['prefix' => 'notes','as'=>'notes.'], function () {
        Route::get('/{faculty_id}', ['uses'=>'NoteController@files','as'=>'index']);
        Route::post('/delete', ['uses'=>'NoteController@delete_file','as'=>'delete']);
        Route::post('/upload', ['uses'=>'NoteController@upload_files','as'=>'upload']);
        Route::get('list_ajax/{faculty_id}', 'NoteController@files_ajax');
    });

    Route::group(['prefix' => 'solutions','as'=>'solutions.'], function () {
        Route::get('/{faculty_id}', ['uses'=>'SolutionController@files','as'=>'index']);
        Route::post('/delete', ['uses'=>'SolutionController@delete','as'=>'delete']);
        Route::post('/upload', ['uses'=>'SolutionController@upload','as'=>'upload']);
    });

});

Route::group(['prefix' => 'profile','as'=>'profile.'], function () {
    Route::get('/{id}', ['uses'=>'UserController@view_profile','as'=>'view']);
});


Route::group(['prefix' => 'user','as'=>'user.'], function () {
    Route::get('/list', ['uses'=>'UserController@index','as'=>'index']);
    Route::post('/register', ['uses'=>'UserController@register','as'=>'register']);
    Route::get('/admin', ['uses'=>'UserController@admin_index','as'=>'index.admin']);
    Route::get('/permission', ['uses'=>'UserController@getPermission','as'=>'permission']);
    Route::Post('/permission/add', ['uses'=>'UserController@savePermission','as'=>'permission.add']);
    Route::get('/assign_permission/{id}', ['uses'=>'UserController@assign_permission', 'as'=>'assign_permission']);
    Route::post('/assign_permission/store', ['uses'=>'UserController@assign_permission_store', 'as'=>'assign_permission_store']);
    Route::post('/permission_view', ['uses'=>'UserController@permission_view', 'as'=>'permission_view']);
});

Route::group(['prefix' => 'job','as'=>'job.'], function () {
    Route::get('/', ['uses'=>'JobController@index','as'=>'index']);
    Route::post('/save', ['uses'=>'JobController@store','as'=>'save']);
    Route::get('/application/{job_id}', ['uses'=>'JobController@get_applications','as'=>'get_applications']);
    Route::post('/update', ['uses'=>'JobController@update','as'=>'update']);
});


Route::get('/setup', ["uses"=>'SetupController@setup'])->name('setup');
Route::post('/setup/run', ["uses"=>'SetupController@setup_run'])->name('setup.run');
