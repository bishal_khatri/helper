<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_users', function (Blueprint $table) {
            $table->bigIncrements('app_user_id');
            $table->string('full_name',255)->nullable();
            $table->string('email')->unique();
            $table->string('password',255)->nullable();
            $table->string('image',255)->nullable();
            $table->string('phone',50)->nullable();
            $table->integer('university_id')->nullable();
            $table->integer('college_id')->nullable();
            $table->integer('faculty_id')->nullable();
            $table->integer('semester')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_users');
    }
}
