<?php

use App\AppUser;
use App\Faculty;
use App\University;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Bishal Khatri';
        $user->email = 'bishal.khatri343@gmail.com';
        $user->password = Hash::make('password');
        $user->user_type = 'admin';
        $user->save();

        $user = new User();
        $user->name = 'User Khatri';
        $user->email = 'bishal.khatri@gmail.com';
        $user->password = Hash::make('password');
        $user->user_type = 'user';
        $user->save();

        $university = new University();
        $university->name = 'Tribhuwan University';
        $university->save();

        $university = new University();
        $university->name = 'Pokhara University';
        $university->save();

        $university = new University();
        $university->name = 'Kathmandu University';
        $university->save();


        $faculty = new Faculty();
        $faculty->university_id = 1;
        $faculty->title = 'Bsc CSIT';
        $faculty->course_type = 'semester';
        $faculty->count = 8;
        $faculty->save();

        $faculty = new Faculty();
        $faculty->university_id = 2;
        $faculty->title = 'BIT';
        $faculty->course_type = 'semester';
        $faculty->count = 8;
        $faculty->save();

        $faculty = new Faculty();
        $faculty->university_id = 3;
        $faculty->title = 'BCA';
        $faculty->course_type = 'year';
        $faculty->count = 4;
        $faculty->save();

        $faculty = new AppUser();
        $faculty->full_name = 'Bishal User Khatri';
        $faculty->email = 'bishal.khatri343@gmail.com';
        $faculty->save();

        $faculty = new AppUser();
        $faculty->full_name = 'Bishal Auth Khatri';
        $faculty->email = 'bishal.khatri@gmail.com';
        $faculty->password = Hash::make('password');
        $faculty->save();
    }
}
