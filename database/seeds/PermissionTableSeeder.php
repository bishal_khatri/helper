<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            // OFFERS START
            [
                'display_name' => 'Offer List',
                'codename' => 'offer-list',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'display_name' => 'Offer Add',
                'codename' => 'offer-add',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'display_name' => 'Offer Applications List',
                'codename' => 'offer-applications-list',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'display_name' => 'Offer Edit',
                'codename' => 'offer-edit',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'display_name' => 'Offer Delete',
                'codename' => 'offer-delete',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            // OFFERS END

            // UNIVERSITY START
            [
                'display_name' => 'University List',
                'codename' => 'university-list',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            // UNIVERSITY END

        ];

        foreach($permissions as $value){
            DB::table('auth_permissions')->insert([
                'display_name' => $value['display_name'],
                'codename' => $value['codename'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at'],
            ]);
        }
    }
}
