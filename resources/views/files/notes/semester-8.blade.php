@extends('layouts.app')
@section('page_title')Notes @stop
@section('page')Notes @stop
@section('css')

@stop
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ $faculty->title ?? '' }} <span class="label label-default text-dark">{{ $faculty->university->name ?? '' }}</span> </h3>
                <br>
                @if ($faculty->course_type=='semester')
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            {{--<semister-8 :faculty-id="'{!! json_encode($faculty->f_id) !!}'"></semister-8>--}}
                            <div class="manage-users">
                                <div class="sttabs tabs-style-iconbox">
                                    <nav>
                                        <ul>
                                            <li>
                                                <a href="#1" class="sticon ti-check-box">
                                                    <span>First Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[1])) {{$notes[1]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#2" class="sticon ti-check-box">
                                                    <span>Second Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[2])) {{$notes[2]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#3" class="sticon ti-check-box">
                                                    <span>Third Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[3])) {{$notes[3]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#4" class="sticon ti-check-box">
                                                    <span>Fourth Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[4])) {{$notes[4]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#5" class="sticon ti-check-box">
                                                    <span>Fifth Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[5])) {{$notes[5]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#6" class="sticon ti-check-box">
                                                    <span>Sixth Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[6])) {{$notes[6]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#7" class="sticon ti-check-box">
                                                    <span>Seventh Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[7])) {{$notes[7]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#8" class="sticon ti-check-box">
                                                    <span>Eighth Semester
                                                        <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($notes[8])) {{$notes[8]->count() }} @else 0 @endif</span></sup>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                    <div class="content-wrap">
                                        <section id="1">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">First Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="1" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[1]))
                                                        @foreach($notes[1] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small>
                                                                </td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#edit_file" data-data="{{ $value }}" data-toggle="modal" class="btn btn-sm btn-link action-primary text-primary"><i class="mdi mdi-pencil-box-outline"></i></a>
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="2">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Second Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="2" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[2]))
                                                        @foreach($notes[2] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="3">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Third Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="3" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[3]))
                                                        @foreach($notes[3] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="4">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Fourth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="4" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[4]))
                                                        @foreach($notes[4] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="5">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Fifth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="5" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[5]))
                                                        @foreach($notes[5] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="6">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Sixth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="6" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[6]))
                                                        @foreach($notes[6] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="7">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Seventh Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="7" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[7]))
                                                        @foreach($notes[7] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="8">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Eighth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="8" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Notes</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">VIEWS</th>
                                                        <th class="text-center">DOWNLOADS</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($notes[8]))
                                                        @foreach($notes[8] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td class="text-center">{{ $value->views }}</td>
                                                                <td class="text-center">{{ $value->downloads }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small>
                                                                </td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="8"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                    </div>
                                    <!-- /content -->
                                </div>
                                <!-- /tabs -->
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @include('files.notes.add-modal')
    @include('files.notes.edit-modal')
    @include('files.notes.delete-modal')
@endsection

@section('script')
    <script src="{{ asset(STATIC_DIR.'assets/js/cbpFWTabs.js') }}"></script>
    <script type="text/javascript">
        (function() {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();

        $('#exampleModal').on('shown.bs.modal', function(e){
            var button = $(e.relatedTarget);
            var semester = button.data('semester');
            var faculty_id = button.data('faculty');

            $(".semester").val(semester);
            $(".faculty_id").val(faculty_id);
        });

        // AJAX FILE DELETE
        $('#edit_file').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var data = button.data('data');
            // console.log(data)
            $("#edit_file_id").val(data.n_id);
            $("#semester").val(data.semester);
            $("#title").val(data.title);
            $("#subject_code").val(data.subject_code);
        });

        // AJAX FILE DELETE
        $('#delete_file').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#file_id").val(id);
        });

        $('#delete_file').find('.modal-footer #confirm').on('click', function () {
            var id = $("#file_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('files.notes.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
        $(document).ready(function () {
            $(".select_file_type").on('change', function (e) {
                console.log(e);
            });

            // MULTIPLE FILE ATTRIBUTE
            $(".count_check").on('change', function(e){
                if ($('.count_check').prop('checked')) {
                    $(".count").html("MULTIPLE")
                    $(".upload_file").prop("multiple", true);
                }else{
                    $(".count").html("SINGLE")
                    $(".upload_file").removeAttr("multiple");
                }
            });

            $(".count").html("SINGLE");

            $('.upload_file').change(function(e){
                var fileName = e.target.files;
                // alert(fileName.length)
                $(".file_name").empty()
                for (i = 0; i < fileName.length; i++) {
                    $(".file_name").append("<p>"+ parseInt(i+1) + " " + fileName[i].name +"</p>");
                }

            });
        });
    </script>
@stop
