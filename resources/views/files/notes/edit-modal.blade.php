<div class="modal fade bs-example-modal-lg" id="edit_file" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Edit Notes</h4>
            </div>
            <form method="post" action="{{ route('files.notes.upload') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="file_id" id="edit_file_id">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Semester:</label>
                        <input type="text" readonly name="semester" class="semester form-control" id="semester">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Title:</label>
                        <input type="text" name="title" class="form-control" placeholder="Enter file title" id="title" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Subject Code:</label>
                        <input type="text" name="subject_code" class="form-control" placeholder="Enter subject code" id="subject_code">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">File Type:</label>
                        <select class="form-control select_file_type" id="" required>
                            <option value="">SELECT FILE TYPE</option>
                            <option value="pdf" selected>PDF</option>
                            {{--<option value="image">IMAGES</option>--}}
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="message-text" class="control-label">File: [ <span class="count"></span> ] </label>
                                <input type="file" class="upload_file" name="file[]"><input type="checkbox" class="count_check"> Multiple
                            </div>
                            <div class="col-md-6">
                                <span class="file_name"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect text-left">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
