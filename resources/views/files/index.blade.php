@extends('layouts.app')
@section('page_title')Notes @stop
@section('page')Notes @stop
@section('css')
    <style>
        .collapsible {
            cursor: pointer;
            padding-right: 20px;
            border: none;
        }
        .content {
            padding-top: 20px;
            display: none;
            overflow: hidden;
        }
        .myadmin-dd-empty .dd-list button {
            width: auto;
            height: auto;
            font-size: 12px;
            margin-top: 12px;
        }
        .list-group-item{
            border: 1px solid #2cabe3 !important;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">university & Faculty list </h3><br>
                <div class="table-responsive">
                    <ul class="list-group list-group-full">
                        @if($university->count()>0)
                            @foreach($university as $value)
                                <li class="list-group-item" style="margin-bottom: 3px;">
                                    <strong>{{ $value->name ?? '' }}</strong>
                                    <a class="collapsible pull-right "><i id="menu-edit-icon" class="fa fa-caret-down"></i></a>
                                    <div class="content" style="display: block">
                                        @if ($value->faculty->count() > 0)
                                            @foreach($value->faculty as $val)
                                                <p>
                                                    Faculty : <span style="margin-right:5px;">{{ $val->title }} </span>
                                                    <a  href="{{ route('files.notes.index',$val->f_id) }}" class="btn btn-default btn-sm">Notes</a>
                                                    <a  href="{{ route('files.solutions.index',$val->f_id) }}" class="btn btn-default btn-sm">Solution</a>
                                                </p>
                                            @endforeach
                                        @endif
                                    </div>
                                </li>
                                <br>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        // var coll = document.getElementsByClassName("collapsible");
        // var i;
        //
        // for (i = 0; i < coll.length; i++) {
        //     coll[i].addEventListener("click", function() {
        //         this.classList.toggle("active");
        //         var content = this.nextElementSibling;
        //         if (content.style.display === "block") {
        //             content.style.display = "none";
        //         } else {
        //             content.style.display = "block";
        //         }
        //     });
        // }

        $(".select_file_type").on('change', function(){
            console.log(this.val())
        })
    </script>
@stop
