<div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Add Notes</h4>
            </div>
            <form method="post" action="{{ route('files.solutions.upload') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="faculty_id" class="faculty_id" id="">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Semester:</label>
                        <input type="text" readonly name="semester" class="semester form-control" id="recipient-name1">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Title:</label>
                        <input type="text" name="title" class="form-control" placeholder="Enter file title" id="recipient-name1" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Subject Code:</label>
                        <input type="text" name="subject_code" class="form-control" placeholder="Enter subject code" id="recipient-name1">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">File:</label>
                        <input type="file" name="file" id="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect text-left">Upload</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
