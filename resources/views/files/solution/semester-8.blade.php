@extends('layouts.app')
@section('page_title')Solutions @stop
@section('page')Solutions @stop
@section('css')
    <style>
        .manage-users .tabs-style-iconbox nav {
            background: #108627;
        }
        .tabs-style-iconbox nav ul li.tab-current a {
            background: #10b027;
            color: #fff;
            box-shadow: -1px 0 0 #fff;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">{{ $faculty->title ?? '' }} <span class="label label-default text-dark">{{ $faculty->university->name ?? '' }}</span> </h3>
                <br>
                @if ($faculty->course_type=='semester')
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="manage-users">
                                <div class="sttabs tabs-style-iconbox">
                                    <nav>
                                        <ul>
                                            <li><a href="#1" class="sticon ti-check-box"><span>First Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[1])) {{$solutions[1]->count() }} @else 0 @endif</span></sup></span></a></li>
                                            <li><a href="#2" class="sticon ti-check-box"><span>Second Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[2])) {{$solutions[2]->count() }} @else 0 @endif</span></sup></span></a></li>
                                            <li><a href="#3" class="sticon ti-check-box"><span>Third Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[3])) {{$solutions[3]->count() }} @else 0 @endif</span></sup></span></a></li>
                                            <li><a href="#4" class="sticon ti-check-box"><span>Fourth Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[4])) {{$solutions[4]->count() }} @else 0 @endif</span></sup></span></a></li>
                                            <li><a href="#5" class="sticon ti-check-box"><span>Fifth Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[5])) {{$solutions[5]->count() }} @else 0 @endif</span></sup></span></a></li>
                                            <li><a href="#6" class="sticon ti-check-box"><span>Sixth Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[6])) {{$solutions[6]->count() }} @else 0 @endif</span></sup></span></a></li>
                                            <li><a href="#7" class="sticon ti-check-box"><span>Seventh Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[7])) {{$solutions[7]->count() }} @else 0 @endif</span></sup></span></a></li>
                                            <li><a href="#8" class="sticon ti-check-box"><span>Eighth Semester <sup><span class="badge badge-success" style="padding-top: 5px;">@if (isset($solutions[8])) {{$solutions[8]->count() }} @else 0 @endif</span></sup></span></a></li>
                                        </ul>
                                    </nav>
                                    <div class="content-wrap">
                                        <section id="1">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">First Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="1" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[1]))
                                                        @foreach($solutions[1] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="2">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Second Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="2" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[2]))
                                                        @foreach($solutions[2] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="3">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Third Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="3" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[3]))
                                                        @foreach($solutions[3] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="4">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Fourth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="4" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[4]))
                                                        @foreach($solutions[4] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="5">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Fifth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="5" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[5]))
                                                        @foreach($solutions[5] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="6">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Sixth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="6" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[6]))
                                                        @foreach($solutions[6] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="7">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Seventh Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="7" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[7]))
                                                        @foreach($solutions[7] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section id="8">
                                            <div class="p-20 row">
                                                <div class="col-sm-6">
                                                    <h3 class="m-t-0">Eighth Semester</h3>
                                                </div>
                                                <div class="col-sm-6">
                                                    <ul class="side-icon-text pull-right">
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#exampleModal" data-semester="8" data-faculty="{{ $faculty->f_id }}">
                                                                <span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span>Add Solution</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="table-responsive manage-table">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">TITLE</th>
                                                        <th class="text-center">SUBJECT CODE</th>
                                                        <th class="text-center">FILE</th>
                                                        <th class="text-center">SEMESTER</th>
                                                        <th class="text-center">CREATED AT</th>
                                                        <th class="text-center" style="width: 150px;">ACTION</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(isset($solutions[8]))
                                                        @foreach($solutions[8] as $key=>$value)
                                                            <tr class="advance-table-row">
                                                                <td class="text-center">{{ $value->title }}</td>
                                                                <td class="text-center">{{ $value->subject_code }}</td>
                                                                <td class="text-center"><a href="{{ asset(STATIC_DIR.'storage/'.$value->file) }}" target="_blank" class="btn btn-link btn-sm">view</a></td>
                                                                <td class="text-center">{{ $value->semester }}</td>
                                                                <td style="width: 200px;">
                                                                    {{ $value->created_at->format('d M Y') }}
                                                                    <small style="font-size: 10px;font-style: italic">{{ $value->created_at->diffForHumans() }}</small></td>
                                                                <td class="text-center" style="width: 150px;">
                                                                    <a href="#delete_file" data-id="{{ $value->n_id }}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" class="sm-pd"></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr class="advance-table-row"><td colspan="6"><p>No items found</p></td></tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                    </div>
                                    <!-- /content -->
                                </div>
                                <!-- /tabs -->
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @include('files.solution.add-modal')
    @include('files.solution.delete-modal')
@endsection

@section('script')
    <script src="{{ asset(STATIC_DIR.'assets/js/cbpFWTabs.js') }}"></script>
    <script type="text/javascript">
        (function() {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();

        $('#exampleModal').on('shown.bs.modal', function(e){
            var button = $(e.relatedTarget);
            var semester = button.data('semester');
            var faculty_id = button.data('faculty');

            $(".semester").val(semester);
            $(".faculty_id").val(faculty_id);
        });

        // AJAX FILE DELETE
        $('#delete_file').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#file_id").val(id);
        });

        $('#delete_file').find('.modal-footer #confirm').on('click', function () {
            var id = $("#file_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('files.solutions.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
    </script>
@stop
