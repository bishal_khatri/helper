@extends('layouts.app')

@section('page_title')
    Profile
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('page')
    User Profile
@endsection

@section('content')


    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">

                <div class="user-bg"> <img width="100%" alt="user" src="{{asset(STATIC_DIR.'assets/plugins/images/large/img1.jpg')}}">
                    <div class="overlay-box">
                        <div class="user-content">

                            <a href="javascript:void(0)">
                                @if($user->image == null)
                                    <img src="{{ asset(STATIC_DIR.'assets/icons/default-user.png') }}" class="thumb-lg img-circle" alt="img">
                                @else
                                    <img src="{{ asset(STATIC_DIR.'storage/'.$user->image) }}" class="thumb-lg img-circle" alt="img">
                                @endif
                            </a>
                            <h4 class="text-white">{{$user->name}}</h4>
                            <h5 class="text-white">{{$user->email}}</h5> </div>
                    </div>
                </div>

                {{--<div class="user-btm-box">--}}
                    {{--<div class="col-md-4 col-sm-4 text-center">--}}
                        {{--<p class="text-purple"><i class="ti-facebook"></i></p>--}}
                        {{--<h1>258</h1> </div>--}}
                    {{--<div class="col-md-4 col-sm-4 text-center">--}}
                        {{--<p class="text-blue"><i class="ti-twitter"></i></p>--}}
                        {{--<h1>125</h1> </div>--}}
                    {{--<div class="col-md-4 col-sm-4 text-center">--}}
                        {{--<p class="text-danger"><i class="ti-dribbble"></i></p>--}}
                        {{--<h1>556</h1> </div>--}}
                {{--</div>--}}
            </div>
        </div>

        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab">
                        <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Settings</span> </a>
                    </li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="settings">
                        @if($errors->has('pwd_mismatch'))
                            <span class="invalid-feedback" role="alert">
                                            <span class="text-sm text-danger">{{ $errors->first('pwd_mismatch') }}</span>
                            </span>
                        @endif
                        <form class="form-horizontal form-material" action="{{ route('profile-update') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Full Name</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="Enter Name" name="name" value="{{$user->name}}" class="form-control form-control-line">
                                    @if($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="text-sm text-danger">{{ $errors->first('name') }}</span>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" placeholder="Enter Email" value="{{$user->email}}" class="form-control form-control-line" name="example-email" id="example-email" disabled="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Old Password</label>
                                <div class="col-md-12">
                                    <input type="password" name="old_password" name="old_password" placeholder="Enter Old Password" class="form-control form-control-line">
                                    @if($errors->has('old_password'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="text-sm text-danger">{{ $errors->first('old_password') }}</span>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">New Password</label>
                                <div class="col-md-12">
                                    <input type="password" name="password" placeholder="Enter New Password" class="form-control form-control-line">
                                    @if($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="text-sm text-danger">{{ $errors->first('password') }}</span>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-12">Confirm Password</label>
                                <div class="col-md-12">
                                    <input type="password" name="password_confirmation" placeholder="Enter Password Again" class="form-control form-control-line"> </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-6 ol-md-6 col-xs-12">
                                    <div class="white-box">
                                        <h3 class="box-title">Image Upload</h3>
                                        {{--<label for="input-file-now-custom-1">You can add a default value</label>--}}
                                        @if($user->image != null)
                                            <input type="file" name="image" id="input-file-now-custom-1" class="dropify"
                                                data-default-file="{{ asset(STATIC_DIR.'storage/'.$user->image) }}" />
                                        @else
                                            <input type="file" name="image" id="input-file-now-custom-1" class="dropify"/>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10"><i class=" fa fa-check"></i> Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @endsection

@section('script')
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();

            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
            // Us
        });
    </script>
@endsection
