<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">

        </div>
        @if(Session::has('message'))
            @foreach(Session::get('message') as $val)
                {{ $loop->iteration }} => <strong style="color: green;">{{ $val }}</strong><br>
            @endforeach
        @endif
        <br><br>
        <form action="{{ route('setup.run') }}" method="post">
            @csrf
            <label for="">Views CLear</label>
            <input type="checkbox" name="view_clear" id=""><br>
            <label for="">Cache Clear</label>
            <input type="checkbox" name="cache_clear" id=""><br>
            <label for="">Config Cache</label>
            <input type="checkbox" name="config_cache" id=""><br>
            <label for="">Migrate Refresh</label>
            <input type="checkbox" name="migrate_refresh" id=""><br>
            <label for="">Migrate Fresh</label>
            <input type="checkbox" name="migrate_fresh" id=""><br>
            <label for="">Migrate</label>
            <input type="checkbox" name="migrate" id=""><br>
            <label for="">DB Seed</label>
            <input type="checkbox" name="db_seed" id=""><br>
            <label for="">Storage Link</label>
            <input type="checkbox" name="storage_link" id=""><br>
            <label for="">Passport Install</label>
            <input type="checkbox" name="passport_install" id=""><br>
            <label for="">Seed Permission</label>
            <input type="checkbox" name="seed_permission" id=""><br>
            <input type="submit" name="" value="Go" id="">
        </form>
    </div>
</div>
</body>
</html>
