<div class="modal fade bs-example-modal-lg" id="add-offer" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Add Offer</h4>
            </div>
            <form method="post" action="{{ route('offers.save') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="user_type" value="user" id="">
                    <div class="form-group">
                        <label for="input-file-now">Image</label>
                        <input type="file" name="image" id="input-file-now" class="dropify" />
                    </div>
                    <div class="form-group">
                        <label for="input-file-now">Title</label>
                        <input type="text" name="title" value="{{old('title')}}" class="form-control" placeholder="Enter offer title" />
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Offer Start - End date <span class="text-warning">( optional )</span></label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" class="form-control" name="start" value="{{old('start')}}" placeholder="Start date" /> <span class="input-group-addon bg-info b-0 text-white">to</span>
                            <input type="text" class="form-control" name="end"  value="{{old('end')}}" placeholder="End date"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-file-now">Description <span class="text-warning">( optional )</span></label>
                        <textarea name="description" class="form-control textarea_editor" placeholder="Enter description (Optional)" id="" cols="30" rows="10">{{old('description')}}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
