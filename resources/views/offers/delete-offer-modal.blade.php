<div class="modal fade bs-example-modal-sm" id="delete_offer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="mySmallModalLabel"><i class="mdi mdi-information text-danger fa-fw"></i>Delete</h4> </div>
            <div class="modal-body">
                Are you sure you want to delete ?
            </div>
            <input type="hidden" name="" id="offer_id">
            <div class="modal-footer">
                <button type="button" class="btn btn-danger green confirm" id="confirm"> Delete
                </button>
                <button type="button" class="btn btn-defult" data-dismiss="modal">
                    Cancel
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
