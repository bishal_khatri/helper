<div class="modal fade bs-example-modal-lg" id="edit-offer" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Edit Offer</h4>
            </div>
            <form method="post" action="{{ route('offers.edit') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="offer_id" id="offer_id">

                    <div class="form-group">
                        <label for="input-file-now">Image</label>
                        <input type="file" name="image" id="input-file-now" class="dropify" />
                    </div>

                    <div class="form-group">
                        <label for="input-file-now">Title</label>
                        <input type="text" name="title" id="title" value="{{old('title')}}" class="form-control" placeholder="Enter offer title" />
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Offer Start - End date <span class="text-warning">( optional )</span></label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" class="form-control" name="start" id="start_date" value="{{old('start')}}" placeholder="Start date" /> <span class="input-group-addon bg-info b-0 text-white">to</span>
                            <input type="text" class="form-control" name="end"  id="end_date" value="{{old('end')}}" placeholder="End date"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="input-file-now">Description <span class="text-warning">( optional )</span></label>
                        <textarea name="description" id="" class="form-control textarea_editor1" placeholder="Enter description (Optional)" cols="30" rows="10"><span class="description"></span></textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
