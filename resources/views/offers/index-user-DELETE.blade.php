@extends('layouts.app')
@section('page_title')Offers @stop
@section('page')Offers @stop
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css') }}" />
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">My Offers </h3>
                <div class="table-responsive"><a class="btn btn-sm btn-default pull-right" href="#add-offer" data-toggle="modal">Add</a>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 350px;">TITLE</th>
                            <th>IMAGE</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th>VALIDITY</th>
                            <th>STATUS</th>
                            <th>DESCRIPTION</th>
                            <th>APPLICATIONS</th>
                            <th>OFFERED BY</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($offers->count() > 0)
                        @foreach ($offers as $value)
                            <tr class="title">
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ $value->title ?? '' }}
                                    <div class="action">
                                        @if ($value->can_edit==1)
                                            <a class="btn btn-sm btn-link text-blue" href="#edit-offer"
                                               data-toggle="modal" data-id="{{$value->id}}"
                                               data-title="{{$value->title}}" data-description="{{$value->description}}"
                                               data-start_date="{{$value->start_date}}" data-end_date="{{$value->end_date}}">Edit</a>
                                        @else
                                            <a href="" class="btn btn-sm btn-link text-blue">request edit</a>
                                        @endif
                                        <span class="vl"></span>
                                        <a href="#delete_offer" data-id="{{$value->id}}" data-toggle="modal" class="btn btn-sm btn-link text-danger">Trash</a>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{ asset(STATIC_DIR.'storage/'.$value->image) }}" target="_blank"><i class="mdi mdi-image"></i></a>
                                </td>
                                <td>@if(!empty($value->start_date)) {{ $value->start_date->format('d M Y') }} @else - @endif</td>
                                <td>@if(!empty($value->end_date)) {{ $value->end_date->format('d M Y') }} @else - @endif</td>
                                <td>
                                    @if (!empty($value->end_date) AND $value->end_date->isPast())
                                        <span class="label label-danger">EXPIRED</span>
                                    @else
                                        <span class="label label-success">ACTIVE</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($value->is_approved==0)
                                        <span class="label label-danger">PENDING</span>
                                    @elseif ($value->is_approved==1)
                                        <span class="label label-danger">APPROVED</span>
                                    @else
                                        <span class="label label-success">REJECTED</span>
                                    @endif
                                </td>
                                <td>{{ $value->description ?? '' }}</td>
                                <td><a href="{{ route('offers.get_applications',$value->id) }}">[ {{ $value->application->count() }} ] application/s</a></td>
                                <td><a href="{{ route('profile.view',$value->offer_by) }}" calss="btn btn-link btn-sm">{{ $value->offer->name }}</a></td>
                            </tr>
                        @endforeach
                        @else
                            <tr>
                                <td colspan="9">No offers found. Click <a class="btn btn-sm btn-linkt" href="#add-offer" data-toggle="modal">here </a>to add new offer.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('offers.add-offer-modal')
    @include('offers.edit-offer-modal')
    @include('offers.delete-offer-modal')

@endsection

@section('script')
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') }} "></script>
    <script>
        // Date Picker
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
        // Daterange picker
        $('.input-daterange-datepicker').daterangepicker({
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-danger',
            cancelClass: 'btn-inverse'
        });

        $(document).ready(function() {
            $('.dropify').dropify();
            $('.textarea_editor').wysihtml5();
        });

        // OFFER EDIT
        $('#edit-offer').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var title = button.data('title');
            var description = button.data('description');
            var start_date = button.data('start_date');
            var end_date = button.data('end_date');

            var modal = $(this)

            if(description != ''){
                modal.find('.modal-body #description').val(description)
            }

            if(start_date != ''){
                modal.find('.modal-body #start_date').val(start_date)
            }

            if(end_date != ''){
                modal.find('.modal-body #end_date').val(end_date)
            }

            modal.find('.modal-body #offer_id').val(id)
            modal.find('.modal-body #title').val(title)
            $("#offer_id").val(id);
        });

        // OFFER DELETE
        $('#delete_offer').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#offer_id").val(id);
        });

        $('#delete_offer').find('.modal-footer #confirm').on('click', function () {
            var id = $("#offer_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('offers.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg.success);
                    $("#delete_offer").modal("hide");
                    window.location.reload();
                }
            });
        });
    </script>
@stop
