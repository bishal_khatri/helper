@extends('layouts.app')
@section('page_title')Job Applications @stop
@section('page')Job Applications @stop
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Applications for [ <span class="text-primary">{{ $offer->title ?? '' }}</span> ] </h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">USER ID</th>
                            <th>NAME</th>
                            <th>EMAIL</th>
                            <th class="text-center">CONTACT</th>
                            <th class="text-center">CV</th>
                            <th>APPLIED DATE</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($applications->count() > 0)
                            @foreach ($applications as $value)
                                <tr class="title">
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class="text-center">
                                        {{ $value->app_user_id ?? '-' }}
                                    </td>
                                    <td>
                                        {{ $value->app_user->full_name ?? '-' }}
                                    </td>
                                    <td>{{ $value->app_user->email ?? '-' }}</td>
                                    <td class="text-center">{{ $value->app_user->phone ?? '-' }}</td>
                                    <td class="text-center">
                                        @if(isset($value->app_user->meta->cv))
                                            <a href="{{ asset(STATIC_DIR.'storage/'.$value->app_user->meta->cv) }}" target="_blank" class="btn btn-default btn-sm">View</a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>{{ $value->created_at }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">No applications found.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script>
        // Date Picker
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
        // Daterange picker
        $('.input-daterange-datepicker').daterangepicker({
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-danger',
            cancelClass: 'btn-inverse'
        });

        $(document).ready(function() {
            $('.dropify').dropify();
        });
    </script>
@stop
