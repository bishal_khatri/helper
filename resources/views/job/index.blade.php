@extends('layouts.app')
@section('page_title')Jobs / Interns @stop
@section('page')Jobs / Interns @stop
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <link href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css') }}" />
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Jobs / Intern Listing <a class="btn btn-sm btn-default pull-right" href="#add-job" data-toggle="modal">Add</a></h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th style="width: 400px;">TITLE</th>
                            <th class="text-center">DESCRIPTION</th>
                            <th class="text-center">DEADLINE</th>
                            <th class="text-center">TYPE</th>
                            <th class="text-center">DESIGNATION</th>
                            <th class="text-center">LOCATION</th>
                            <th class="text-center">APPLICATIONS</th>
                            <th class="text-center">PROVIDER</th>
                            <th class="text-center">CREATED AT</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($jobs->count() > 0)
                        @foreach ($jobs as $value)
                            <tr class="title">
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>
                                    {{ $value->title ?? '' }}
                                    @if (!empty($value->deadline) AND $value->deadline->isPast())
                                        <span class="label label-danger">EXPIRED</span>
                                    @else
                                        <span class="label label-success">ACTIVE</span>
                                    @endif
                                    <div class="action">
                                        <a href="#edit-job" class="btn btn-sm btn-link text-blue" data-toggle="modal" data-data="{{ $value }}">Edit</a>
                                        <span class="vl"></span>
                                        <a href="" class="btn btn-sm btn-link text-danger">Trash</a>
                                    </div>
                                </td>

                                <td class="text-center">
                                    @if (!empty($value->description))
                                        <a class="btn btn-default btn-sm" href="#view-description" data-toggle="modal"
                                           data-title="{{ $value->title }}"
                                           data-data="{!! $value->description !!}">View</a>
                                    @else
                                        -
                                    @endif
                                </td>

                                
                                <td class="text-center">
                                    {{ $value->deadline->format('d M Y') }}
                                </td>

                                <td class="text-center"><span class="label label-primary">{{ $value->job_type }}</span></td>
                                <td class="text-center">{{ $value->designation }}</td>
                                <td class="text-center">    
                                     {{  $value->location  }}           
                                </td>
                                <td class="text-center"><a href="{{ route('job.get_applications',$value->id) }}">[ {{ $value->applications->count() }} ] application/s</a></td>
                                <td class="text-center">
                                    <a href="{{ route('profile.view',$value->provider) }}" calss="btn btn-link btn-sm">{{ $value->job_provider->name }}</a>
                                </td>
                                <td class="text-center">
                                    {{ $value->created_at->format('d M Y') }}
                                </td>
                            </tr>
                        @endforeach
                        @else
                            <tr>
                                <td colspan="7">No offers found. Click <a class="btn btn-sm btn-linkt" href="#add-job" data-toggle="modal">here </a>to add new job / intern.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('job.add-job-modal')
    @include('job.edit-job-modal')
    @include('job.description-modal')
@endsection

@section('script')
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') }} "></script>
    <script>
        $(document).ready(function() {
            $('.textarea_editor').wysihtml5();

        });
        jQuery('.datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });

        $(document).ready(function() {
            $('.dropify').dropify();
        });

        $('#view-description').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var title = button.data('title');
            var description = button.data('data');
            $(".job-title").html(title);
            $(".description").html(description);
        });
        $('#edit-job').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var data = button.data('data');

            $("#edit_id").val(data.id);
            $("#edit_title").val(data.title);
            $("#edit_deadline").val(data.deadline);
            $("#edit_job_type").val(data.job_type);
            $("#edit_provider").val(data.provider);
            $("#edit_designation").val(data.designation);
            $("#edit_location").val(data.location);

            // if($('#edit_job_type').val() == "job"){
                    
            // }

        });
    </script>
@stop
