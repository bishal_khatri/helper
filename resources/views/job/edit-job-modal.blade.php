<div class="modal fade bs-example-modal-lg" id="edit-job" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Add Job / Intern</h4>
            </div>
            <form method="post" action="{{ route('job.update') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="job_id" id="edit_id">
                    {{--<div class="form-group">--}}
                        {{--<label for="input-file-now">Image</label>--}}
                        {{--<input type="file" name="image" id="input-file-now" class="dropify" />--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label for="input-file-now">Title</label>
                        <input type="text" name="title" id="edit_title" class="form-control" placeholder="Enter job title" />
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Deadline</label>
                        <div class="input-daterange input-group" id="date-range">
                            <input type="text" id="edit_deadline" class="form-control datepicker-autoclose" name="deadline" placeholder="Deadline" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="input-file-now">Job Location</label>
                        <input type="text" name="location" id="edit_location" class="form-control" placeholder="Enter job location" />
                        @error('location')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="input-file-now">Job Type</label>
                        <input type="text" class="form-control" readonly id="edit_job_type">
                        <select name="job_type" id="" class="form-control">
                            <option value="Internship">Internship</option>
                            <option value="Job">Job</option>
                        </select>
                    </div>
                    @if (Auth::check() AND Auth::user()->user_type=="admin")
                        <div class="form-group">
                            <label for="input-file-now">Provider</label>
                            <input type="text" class="form-control" readonly id="edit_provider">
                            
                            <select name="provider" id="" class="form-control">
                                <option value="Internship">--select--</option>
                                @foreach($providers as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="input-file-now">Designation</label>
                        <input type="text" name="designation" id="edit_designation" class="form-control" placeholder="Enter designation" />
                        @error('designation')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="input-file-now">Description <span class="text-warning">( optional )</span></label>
                        <textarea name="description" class="form-control textarea_editor1" placeholder="Enter description (Optional)" id="" cols="30" rows="10">{{old('description')}}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <span class="text-sm text-danger">{{ $message }}</span>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
