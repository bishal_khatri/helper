<div class="modal fade bs-example-modal-lg" id="add-university" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Add University</h4>
            </div>
            <form method="post" action="{{ route('university.save') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="type" value="university" id="">
                    <div class="form-group">
                        <label for="input-file-now">University Logo</label>
                        <input type="file" name="logo" id="input-file-now" class="dropify" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">University Name</label>
                        <input type="text" name="name" class="form-control" id="" placeholder="University Name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default waves-effect waves-light m-r-10">Reset</button>
                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{--modal for edit the university--}}
<div class="modal fade bs-example-modal-lg" id="edit-university" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Edit University</h4>
            </div>
            <form method="post" action="{{ route('university.save') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="type" value="university" id="">
                    <div class="form-group">
                        <label for="input-file-now">University Logo</label>
                        <input type="file" name="logo" id="input-file-now" class="dropify" src="{{asset(STATIC_DIR.'university/ujBcIY1dzcUGDJxkLkC4F5DqFnsBIlEqetwHJvxG.jpeg" ')}}"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">University Name</label>
                        <input type="text" name="name" class="form-control" id="university_name" placeholder="University Name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{--end edit modal university--}}
