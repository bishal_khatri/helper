<div class="modal fade bs-example-modal-lg" id="add-faculty" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Add Faculty</h4>
            </div>
            <form method="post" action="{{ route('university.save') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="type" value="faculty" id="">
                    <div class="form-group">
                        <label for="input-file-now-custom-2">University</label>
                        <select class="form-control" name="university_id" id="">
                            <option value="">Select One</option>
                            @foreach($university as $value)
                                <option value="{{ $value->u_id }}">{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Faculty Title</label>
                        <input type="text" name="title" class="form-control" id="" placeholder="Faculty Title">
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Course Type</label>
                        <select name="course_type" id="" class="form-control">
                            <option value="">Select One</option>
                            <option value="year">Year</option>
                            <option value="semester">Semester</option>
                        </select>
                        @error('course_type')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Number of Year/Semester</label>
                        <input type="number" name="count" class="form-control" id="" placeholder="Number of Year/Semester">
                        @error('count')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default waves-effect waves-light m-r-10">Reset</button>
                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
