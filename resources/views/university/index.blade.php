@extends('layouts.app')
@section('page_title')University @stop
@section('page')University @stop
@section('css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@stop
@section('content')
    <div class="white-box">
        <div class="row">
            <div class="col-lg-6">
                <h3 class="box-title m-b-2">university list <a class=" btn btn-sm btn-default pull-right" href="#add-university" data-toggle="modal">add</a></h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center">NAME</th>
                            <th class="text-center">LOGO</th>
                            <th class="text-center" style="width: 200px;">STATUS</th>
                            <th style="width: 150px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($university as $value)
                            <tr>
                                <td class="text-center">{{ $value->name ?? '' }}</td>
                                <td class="text-center">
                                    @if (!empty($value->logo))
                                        <img src="{{ asset(STATIC_DIR.'storage/'.$value->logo) }}" alt="" width="70">
                                    @else
                                        <i class="mdi mdi-image"></i>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if($value->status==1)
                                        <span class="label label-success">ENABLED</span>
                                        <a href="{{ route('university.change_status',$value->u_id) }}" class="btn btn-link btn-sm">Disable?</a>
                                    @else
                                        <span class="label label-danger">DISABLED</span>
                                        <a href="{{ route('university.change_status',$value->u_id) }}" class="btn btn-link btn-sm">Enable?</a>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="#edit-university" data-id="{{$value->u_id}}" data-name="{{$value->name}}" data-logo="{{$value->logo}}"  data-toggle="modal" class="btn btn-sm btn-link action-primary"><i class="mdi mdi-pencil-box-outline"></i></a>
                                    <span class="vl"></span>
                                    <a href="#delete_university" data-id="{{$value->u_id}}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-6">
                <h3 class="box-title m-b-2">Faculty List <a class=" btn btn-sm btn-default pull-right" href="#add-faculty" data-toggle="modal">add</a></h3>
                <div class="table-responsive">
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center">TITLE</th>
                            <th class="text-center">UNIVERSITY</th>
                            <th class="text-center">COURSE TYPE</th>
                            <th class="text-center">SEM / YEAR</th>
                            <th class="text-center">STATUS</th>
                            <th style="width: 150px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($faculty as $value)
                            <tr>
                                <td class="text-center">{{ $value->title ?? '' }}</td>
                                <td class="text-center">
                                    <span class="label label-default text-dark">{{ $value->university->name }}</span>
                                </td>
                                <td class="text-center">{{ $value->course_type }}</td>
                                <td class="text-center">{{ $value->count }}</td>
                                <td class="text-center">
                                    @if($value->status==1)
                                        <span class="label label-success">ENABLED</span>
                                        <a href="{{ route('faculty.change_status',$value->f_id) }}" class="btn btn-link btn-sm">Disable?</a>
                                    @else
                                        <span class="label label-danger">DISABLED</span>
                                        <a href="{{ route('faculty.change_status',$value->f_id) }}" class="btn btn-link btn-sm">Enable?</a>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <a href="#edit-faculty" data-data="{{$value}}" data-toggle="modal" class="btn btn-sm btn-link action-primary"><i class="mdi mdi-pencil-box-outline"></i></a>
                                    <span class="vl"></span>
                                    <a href="#delete_faculty" data-id="{{$value->f_id}}" data-toggle="modal" class="btn btn-sm btn-link action-danger text-danger"><i class="mdi mdi-delete-forever"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('university.add-university-modal')
    @include('university.add-faculty-modal')
    @include('university.edit-faculty-modal')

    @include('university.delete-university-modal')
    @include('university.delete-faculty-modal')

@endsection

@section('script')
    <script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.dropify').dropify();
        });

        // AJAX FILE UNIVERSITY

        // UNIVERSITY DELETE
        $('#delete_university').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#university_id").val(id);
        });

        $('#delete_university').find('.modal-footer #confirm').on('click', function () {
            var id = $("#university_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('university.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    console.log(msg.success);
                    $("#confirmDelete").modal("hide");
                    // window.location.reload();
                }
            });
        });

        // UNIVERSITY EDIT
        $('#edit-university').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var name = button.data('name');
            var logo = button.data('logo');

            $("#university_id").val(id);
            $("#university_name").val(name);
            $("#university_logo").val(logo);

        });

        $('#delete_university').find('.modal-footer #confirm').on('click', function () {
            var id = $("#university_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('university.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    console.log(msg.success);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });

        // AJAX FILE FACULTY DELETE
        $('#delete_faculty').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $("#faculty_id").val(id);
        });

        $('#delete_faculty').find('.modal-footer #confirm').on('click', function () {
            var id = $("#faculty_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('faculty.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    console.log(msg.success);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });

        // EDIT FACULTY
        $('#edit-faculty').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var data = button.data('data');
           console.log(data)
            $('#faculty_id').val(data.f_id)
            $('#title').val(data.title)
            $('#count').val(data.count)
            $('#university_id').val(data.university_id)
            $('#course_type').val(data.course_type)
        });
    </script>
@stop
