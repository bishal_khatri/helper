<div class="modal fade bs-example-modal-lg" id="edit-faculty" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Edit Faculty</h4>
            </div>
            <form method="post" action="{{ route('faculty.update') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="faculty_id" id="faculty_id">
                    <div class="form-group">
                        <label for="input-file-now-custom-2">University</label>
                        <select class="form-control" name="university_id" id="university_id">
                            <option value="">Select One</option>
                            @foreach($university as $value)
                                <option value="{{ $value->u_id }}">{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Faculty Title</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Faculty Title">
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Course Type</label>
                        <select name="course_type" id="course_type" class="form-control">
                            <option value="">Select One</option>
                            <option value="year">Year</option>
                            <option value="semester">Semester</option>
                        </select>
                        @error('course_type')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Number of Year/Semester</label>
                        <input type="number" name="count" class="form-control" id="count" placeholder="Number of Year/Semester">
                        @error('count')
                        <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
