<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html lang="en">

@include('layouts._partials.header')

<body class="fix-sidebar">
<div id="">
    <!-- Preloader -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
    </div>
    <div id="wrapper">
        <!-- Top Navigation -->
    @include('layouts._partials.top-navbar')
    <!-- End Top Navigation -->
        <!-- Left navbar-header -->
    @include('layouts._partials.main-navbar')
    <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <!-- .page title -->
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">@yield('page')</h4>
                    </div>
                    <!-- /.page title -->
                </div>
                <!-- .row -->

            @yield('content')
            <!-- .row -->
            </div>
            <!-- /.container-fluid -->
            @include('layouts._partials.footer')
        </div>
        <!-- /#page-wrapper -->
    </div>
</div>
    @include('layouts._partials.scripts')
</body>

</html>
