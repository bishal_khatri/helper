<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="">
    <title>Document - @yield('page_title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset(STATIC_DIR.'assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    {{--<!-- This is Sidebar menu CSS -->--}}
    <link href="{{ asset(STATIC_DIR.'assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    {{--<!-- This is a Animation CSS -->--}}
    <link href="{{ asset(STATIC_DIR.'assets/css/animate.css') }}" rel="stylesheet">
    {{--<!-- This is a Custom CSS -->--}}
    <link href="{{ asset(STATIC_DIR.'assets/css/style.css') }}" rel="stylesheet">
    {{--<!-- color CSS you can use different color css from css/colors folder -->--}}
    {{--<!-- We have chosen the skin-blue (default.css) for this starter--}}
         {{--page. However, you can choose any other skin from folder css / colors .--}}
         {{---->--}}
    <link href="{{ asset(STATIC_DIR.'assets/css/colors/megna-dark.css') }}" id="theme" rel="stylesheet">
    <link rel="icon" href="{{ asset(STATIC_DIR.'assets/icons/helper32.png') }}">
    {{--<script src="{{ asset(STATIC_DIR.'js/app.js') }}" defer></script>--}}
{{--    <link href="{{ asset(STATIC_DIR.'css/app.css') }}" rel="stylesheet">--}}
    <style>
        .sidebar {
            z-index: 900;
            position: absolute;
            width: 100%;
            top: 60px;
            height: auto;
            overflow: visible;
            transition: .05s ease-in;
        }
        .action-primary:hover{
            border: 1px solid blue;
        }
        .action-danger:hover{
            border: 1px solid red;
        }
        .action{
            display: none;
        }
        .title:hover .action {
            display: block;
            margin-left: -12px;
        }
        .vl {
            border-left: 1px solid green;
            margin-right: 4px;
            margin-left: 2px;
        }
    </style>
    @yield('css')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
