<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <!-- Toggle icon for mobile view -->
        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo" href="{{ route('home') }}">
            {{--<!-- Logo icon image, you can use font-icon also --><b>--}}
            {{--                    <!--This is dark logo icon--><img src="{{ asset(STATIC_DIR.'assets/icons/helper512.png') }}" width="25" alt="home" class="dark-logo" />--}}
            <!--This is light logo icon--><img src="{{ asset(STATIC_DIR.'assets/icons/helper512.png') }}" style="margin-bottom: 6px;padding-right: 10px;" width="45" alt="home" class="light-logo" />
            {{--</b>--}}
            <!-- Logo text image you can use text also -->
                <span class="hidden-xs">
                        {{--<!--This is dark logo text--><img src="{{ asset(STATIC_DIR.'assets/icons/helperwithpen.png') }}" alt="home" class="dark-logo" />--}}
                        <!--This is light logo text--><img src="{{ asset(STATIC_DIR.'assets/icons/helperwithoutpen.png') }}" alt="home" class="light-logo" width="80"/>
                     </span>
            </a>
        </div>
        <!-- /Logo -->

        <!-- This is the message dropdown -->
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)">
                    @if(!empty(Auth::user()->image))
                        <img src="{{ asset(STATIC_DIR.'storage/'.Auth::user()->image) }}" alt="user-img" width="36" class="img-circle">
                    @else
                        <img src="{{ asset(STATIC_DIR.'assets/icons/default-user.png') }}" alt="user-img" width="36" class="img-circle">
                    @endif
                    <b class="hidden-xs">{{ Auth::user()->name }}</b><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    @if (Auth::check())
                        <li>
                            <div class="dw-user-box">
                                <div class="u-img">
                                    @if(!empty(Auth::user()->image))
                                        <img src="{{ asset(STATIC_DIR.'storage/'.Auth::user()->image) }}" alt="user-img" width="36" class="img-circle">
                                    @else
                                        <img src="{{ asset(STATIC_DIR.'assets/icons/default-user.png') }}" alt="user-img" width="36" class="img-circle">
                                    @endif
                                </div>
                                <div class="u-text">
                                    <h4>{{ Auth::user()->name }}</h4>
                                    <p class="text-muted">{{ Auth::user()->email ?? '' }}</p>
                                    <a href="{{route('profile')}}" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                                </div>
                            </div>
                        </li>
                    @endif
                    <li role="separator" class="divider"></li>
                    {{--<li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>--}}
                    {{--<li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>--}}
                    {{--<li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>--}}
                    {{--<li role="separator" class="divider"></li>--}}
                    {{--<li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>--}}
                    {{--<li role="separator" class="divider"></li>--}}
                    <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off text-danger"></i> Logout</a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </ul>
            </li>
        </ul>
    </div>
</nav>
