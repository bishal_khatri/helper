<!-- jQuery -->
<script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset(STATIC_DIR.'assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Sidebar menu plugin JavaScript -->
<script src="{{ asset(STATIC_DIR.'assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="{{ asset(STATIC_DIR.'assets/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset(STATIC_DIR.'assets/js/waves.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ asset(STATIC_DIR.'assets/js/custom.js') }}"></script>
@yield('script')
