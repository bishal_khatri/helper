<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav">
        <div class="sidebar-head">
            <h3>
                <span class="fa-fw open-close">
                    <i class="ti-menu hidden-xs"></i>
                    <i class="ti-close visible-xs"></i>
                </span>
                <span class="hide-menu">Navigation</span>
            </h3>
        </div>
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ route('home') }}" class="@if(Request::is('/')) active @endif"><i  class="mdi mdi-home fa-fw"></i> <span class="hide-menu">Dashboard</span></a>
            </li>
            @can('university-list')
                <li>
                    <a href="{{ route('university.index') }}" class="@if(Request::is('university')) active @endif"><i  class="mdi mdi-school fa-fw"></i> <span class="hide-menu">University</span></a>
                </li>
            @endcan
            <li>
                <a href="{{ route('files.index') }}" class="@if(Request::is('files') OR Request::is('files/*')) active @endif"><i  class="mdi mdi-file-document fa-fw"></i> <span class="hide-menu">Notes & Solutions</span></a>
            </li>
            @can('offer-list')
                <li>
                    <a href="{{ route('offers.index') }}" class="@if(Request::is('offers') OR Request::is('offers/*')) active @endif"><i  class="mdi mdi-cart-outline fa-fw"></i> <span class="hide-menu">Offers</span></a>
                </li>
            @endcan
            <li>
                <a href="{{ route('job.index') }}" class="@if(Request::is('job') OR Request::is('job/*')) active @endif"><i  class="mdi mdi-briefcase fa-fw"></i> <span class="hide-menu">Jobs/ Interns</span></a>
            </li>
            <li>
                <a href="javascript:void(0)" class="waves-effect @if(Request::is('user/*') OR Request::is('user')) active @endif">
                    <i class="mdi mdi-account-box-outline fa-fw"></i>
                    <span class="hide-menu">Users Management<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ route('user.index.admin') }}"><i class="mdi mdi-account fa-fw"></i><span class="hide-menu">Admins</span></a></li>
                    <li><a href="{{ route('user.index') }}"><i class="mdi mdi-account fa-fw"></i><span class="hide-menu">Users</span></a></li>
                    <li><a href="{{ route('user.permission') }}"><i class="mdi mdi-lock fa-fw"></i><span class="hide-menu">Permission</span></a></li>
                </ul>
            </li>
            <li class="devider"></li>
        </ul>
    </div>
</div>
