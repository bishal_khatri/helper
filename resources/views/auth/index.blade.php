@extends('layouts.app')
@section('page_title')User Listing @stop
@section('page')User Listing @stop
@section('css')
    <style>
        .invalid-feedback{
            color: red;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0"> {{ $user_type }} Listing <a class="btn btn-sm btn-default pull-right" href="#add-user" data-toggle="modal">Add</a></h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>FULL NAME</th>
                            <th>EMAIL</th>
                            <th>USER TYPE</th>
                            <th>PERMISSION</th>
                            <th style="width: 90px;">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $value)
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->email }}</td>
                            <td><span class="label label-primary">{{ $value->user_type }}</span></td>
                            <td class="">
                                <a href="#viewPermission" class="btn btn-link btn-sm" data-ids="{{ $value->id }}" data-toggle="modal">View</a>
                                <span class="vl"></span>
                                <a href="{{ route('user.assign_permission',$value->id) }}" class="btn btn-link btn-sm">Set</a>
                            </td>
                            <td style="">
                                <a href=""><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#confirmDelete" data-ids="{{ $value->id }}" data-toggle="modal" data-rel="delete" data-user="{{ $value->name }}">
                                    <i class="fa fa-trash text-danger"></i>
                                </a>
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('auth.model')
    @include('auth.add-modal')
@endsection

@section('script')
    <script>
        $('#add').find('.modal-footer #confirm_yes').on('click', function () {
            $("#add").modal("hide");
            // window.location.reload();
        });

        $('#viewPermission').on('show.bs.modal', function(e){
            $('#list').html('');
            var button = $(e.relatedTarget);
            var id = button.data('ids');

            $.ajax({
                type: "POST",
                url: "{{ route('user.permission_view') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    setTimeout( function () {
                        $('#list').html(msg);
                    },500);
                }
            });
            $('#list').html('<img width="90" style="margin-left:240px;margin-top:20px;" src="{{ asset(STATIC_DIR.'assets/icons/loading.gif') }}"><p style="margin-left:265px;">Fetching....</p>');
        });
    </script>
@stop
