<div class="modal fade bs-example-modal-lg" id="add-user" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Register New User</h4>
            </div>
            <form method="post" action="{{ route('user.register') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label class="control-label">Full Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Full Name" required value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Email Address</label>
                        <input type="email" name="email" class="form-control" placeholder="Enter Email Address" id="recipient-name1" required value="{{ old('email') }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter password" required >
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="control-label">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Enter password confirmation" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Contact (optional)</label>
                        <input type="text" name="contact" class="form-control" placeholder="Enter contact" value="{{ old('contact') }}">
                        @error('contact')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">User Type</label>
                        <select class="form-control" name="user_type" id="" required>
                            <option value="" disabled="" selected>SELECT USER TYPE</option>
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                        </select>
                        @error('user_type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="message-text" class="control-label">Profile Image (optional)</label>
                                <input type="file" name="image" accept=".jpg,.png">
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default waves-effect text-left">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
