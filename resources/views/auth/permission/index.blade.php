@extends('layouts.app')
@section('page_title')Permission @stop
@section('page')Permission @stop
@section('css')

@stop
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="white-box">
                <h3 class="box-title m-b-0"></h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Display Name</th>
                            <th>Code Name</th>
                            <th style="width: 90px;text-align: center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($permissions as $value)
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $value->display_name }}</td>
                            <td>{{ $value->codename }}</td>
                            <td style="text-align: center">
                                <a href=""><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#confirmDelete" data-ids="{{ $value->id }}" data-toggle="modal" data-rel="delete" data-user="{{ $value->display_name }}">
                                    <i class="fa fa-trash text-danger"></i>
                                </a>
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="white-box">
                <h3 class="box-title m-b-0">Add Permission</h3>
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <form method="post" action="{{ route('user.permission.add') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="input-file-now-custom-2">Display Name</label>
                                <input type="text" name="display_name" class="form-control" id="" placeholder="Display Name">
                                @error('display_name')
                                <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Code Name</label>
                                <input type="text" name="codename" class="form-control" id="" placeholder="Code Name">
                                @error('codename')
                                <span class="invalid-feedback" role="alert">
                                    <span class="text-sm text-danger">{{ $message }}</span>
                                </span>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-default waves-effect waves-light m-r-10">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('auth.permission.model')
@endsection

@section('script')

@stop
